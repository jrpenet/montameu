const cardapio = [{
    nomeHamburguer: 'Clone Basiquinho',
    descricao: 'Pão Bola, carne artesanal 100g cada, ovo, queijo, salada e molho da casa.',
    preco: 16,
    foto: './images/img.jpg'
}, {
    nomeHamburguer: 'Clone Simples',
    descricao: 'Pão Bola, ovo, queijo, calabresa, bacon, salada e molho da casa.',
    preco: 20,
    foto: './images/img.jpg'
}, {
    nomeHamburguer: 'Clone de Frango',
    descricao: 'Pão Bola, filé de peito de frango, ovo, queijo, calabresa, bacon, salada e molho da casa.',
    preco: 20,
    foto: './images/img.jpg'
}, {
    nomeHamburguer: 'Clone Bárbara Moraes',
    descricao: 'Pão Bola, coração 100g cada, ovo, queijo, salsichinha à cabidela, salada e molho da casa.',
    preco: 22,
    foto: './images/img.jpg'
}, {
    nomeHamburguer: 'Clone Nivaldão',
    descricao: 'Pão Bola, 2 carnes artesanais 100g cada, ovo, queijo, calabresa, bacon, salada e molho da casa.',
    preco: 24,
    foto: './images/img.jpg'
}, {
    nomeHamburguer: 'Clone X-Tudinho',
    descricao: 'Pão Bola, 2 carnes artesanais 100g cada, filé de peito de frango, ovo, queijo, calabresa, bacon, salada e molho da casa.',
    preco: 26,
    foto: './images/img.jpg'
}, {
    nomeHamburguer: 'Cabo Moraes',
    descricao: 'Pão Bola, 4 carnes artesanais 100g cada, filé de peito de frango, ovo, queijo, calabresa, bacon, presunto, salcichinha à cabidela, salada e molho da casa.',
    preco: 32,
    foto: './images/img.jpg'
}]


const addCard = (lanche) => {
    const divMain = document.createElement('div')
    const titulo = document.createElement('p')
    const descrito = document.createElement('p')
    const imagem = document.createElement('img')
    const select = document.createElement('select')
    const opt = document.createElement('option')
    const opt2 = document.createElement('option')
    const opt3 = document.createElement('option')
    const btn = document.createElement('button')

    titulo.textContent = lanche.nomeHamburguer
    titulo.setAttribute('class', 'tituloHamburguer')
    descrito.textContent = lanche.descricao
    descrito.setAttribute('class', 'descricaoHamburguer')
    imagem.setAttribute('src', lanche.foto)
    imagem.setAttribute('class', 'hamburguerFoto')
    opt.textContent = 1
    opt.setAttribute('value', '1')
    opt2.textContent = 2
    opt2.setAttribute('value', '2')
    opt3.textContent = 3
    opt3.setAttribute('value', '3')
    btn.textContent = 'PEDIR'
    btn.setAttribute('class', 'btnPedir')




    divMain.appendChild(titulo)
    divMain.appendChild(descrito)
    divMain.appendChild(imagem)
    divMain.appendChild(select)
    select.appendChild(opt)
    select.appendChild(opt2)
    select.appendChild(opt3)
    divMain.appendChild(btn)

    btn.addEventListener('click', () => {
        console.log(lanche.nomeHamburguer)
    })

    return divMain
}

cardapio.forEach((lanche) => {
    const hrEl = document.createElement('hr')
    document.querySelector('.addCardapio').appendChild(hrEl)
    document.querySelector('.addCardapio').appendChild(addCard(lanche))
    
})


// const btn = document.querySelector('button')
// const nome = document.querySelector('#nome').value
// const end = document.querySelector('#endereco').value
// const fone = document.querySelector('#fone').value

// btn.addEventListener('click', function(e){
//     // e.preventDefault()
//     cliente.push(nome, end, fone)
//     location.assign(`https://api.whatsapp.com/send?phone=/5581983419971&text=%2ANome%2A%3A%20${cliente[0]}%0A%2AEndereco%2A%3A%20${end}%0A%2ATelefone%2A%3A%20${fone}`)
// })

//console.log(cliente)