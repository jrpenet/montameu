const menu = document.querySelector('.toggle')
const act = document.querySelector('ul')

menu.addEventListener('click', function(){
    menu.classList.toggle('active')
    act.classList.toggle('active')
   
})

const sections = document.querySelectorAll('.js-scroll')

function animaScroll() {
    sections.forEach((section) => {
        const sectionTop = section.getBoundingClientRect().top - (window.innerHeight * 0.75);
        if(sectionTop < 0){
            section.classList.add('ativo')
        }
        else{
            section.classList.remove('ativo')
        }
    })
}

animaScroll()

window.addEventListener('scroll', animaScroll)